﻿using UnityEngine;
using System.Collections;

public class PlayerUnit : Creep {

    // Use this for initialization
    override public void Start()
    {
        radius = gameObject.GetComponent<NavMeshAgent>().radius;
        index = (count++);
        map = GameController.instance.map;
        map.GetComponent<Map>().units.Add(this);
        foreach (Minimap minimap in map.GetComponent<Map>().minimaps)
        {
            if(minimap!=GameController.instance.bigMinimap)
            {
                GameObject tracker = Instantiate(trackerPrefab);
                tracker.transform.parent = minimap.transform.FindChild("Units");
                tracker.GetComponent<Tracker>().unit = this;
            }
            else
            {
                Debug.Log("Caught the big map I'm not supposed to put a prefab on");
            }
        }
    }

    // Update is called once per frame
    void Update () {
	
	}
}
