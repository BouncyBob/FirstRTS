﻿using UnityEngine;
using System.Collections;

public class Creep : Unit {

    public enum Type {INFANTRY,TANK,NOTHING };
    public static Type[] types = { Type.INFANTRY, Type.TANK,Type.NOTHING };

    public Type type;


	// Use this for initialization
	override public void Start () {
        base.Start();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void Move(Vector3 pos)
    {
        gameObject.GetComponent<NavMeshAgent>().SetDestination(pos);
    }

}
