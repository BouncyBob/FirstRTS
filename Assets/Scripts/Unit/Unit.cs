﻿using UnityEngine;
using System.Collections;

public class Unit : MonoBehaviour {

    ArrayList trackers;

    public Map map;
    public GameObject trackerPrefab;
    public GameObject worldPrefab;
    public double radius;
    public bool selected;
    public static int count=0;
    public int index;

    public Player player;

    public string buildName;
    public float buildTime;
    public int cost;
    public int health;

    public Building[] requisites;

	// Use this for initialization
	virtual public void Start () {
        requisites =new Building[0];
        index = (count++);
        map = GameController.instance.map;
        map.units.Add(this);
	    foreach(Minimap minimap in map.minimaps)
        {
            minimap.SpawnUnit(this);
            /*
            GameObject tracker = Instantiate(trackerPrefab);
            tracker.transform.parent = minimap.transform.FindChild("Units");
            tracker.GetComponent<Tracker>().unit = gameObject;
            */
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        
	}

    public virtual void Move(Vector3 pos)
    {

    }


    public static bool canSpawn(GameObject prefab, Vector3 globalPos)
    {
        //if this isn't a spot on the map
        NavMeshHit nav;
        if (!NavMesh.SamplePosition(globalPos, out nav, 1f, NavMesh.AllAreas))
        {
            return false;
        }
        Vector3 actualPos = nav.position;
        if (!NavMesh.FindClosestEdge(actualPos, out nav, NavMesh.AllAreas))
        {
            return false;
        }
        //if anyone's in the way
        foreach (Unit unit in GameController.instance.map.units)
        {
            if (Vector3.Distance(unit.transform.localPosition, actualPos) < (prefab.GetComponent<Unit>().radius + unit.GetComponent<Unit>().radius))
            {
                return false;
            }
        }
        float navmeshRadius = GameController.instance.navmeshRadius;
        if (Vector3.Distance(nav.position, actualPos) <= prefab.GetComponent<Unit>().radius - navmeshRadius)
        {
            return false;
        }
        return true;
    }
}
