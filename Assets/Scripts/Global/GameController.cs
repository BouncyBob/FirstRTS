﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

    public Map map;
    public float navmeshRadius;
    public GameObject playerUnit;
    public GameObject defaultSpawns;
    public GameObject cursorPrefab;
    public GameObject minimapPrefab;
    public GameObject bigMinimap;
    public ArrayList minimaps;
    public GameObject playerCabin;
    public GameObject[] buildingPrefabs;
    public GameObject[] creepPrefabs;
    public static GameController instance;
    public GameObject[] buttonPrefabs;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

	// Use this for initialization
	void Start () {
        Instantiate(playerUnit,Vector3.zero,Quaternion.identity,map.transform.FindChild("Units"));	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
