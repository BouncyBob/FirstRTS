﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Map : MonoBehaviour {

    public List<Minimap> minimaps;
    public List<Unit> units;

	// Use this for initialization
	void Start () {
        for(int i = 0; i < transform.FindChild("Geometry").childCount; i++)
        {
           //transform.GetChild(0).GetChild(i).GetComponent<MeshRenderer>().enabled = false;
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    internal GameObject create(GameObject spawn, Vector3 pos, Quaternion rot)
    {
        GameObject unit = (GameObject)Instantiate(spawn, transform.TransformPoint(pos), rot);
        unit.transform.parent = transform.FindChild("Units");
        unit.transform.localPosition = pos;
        unit.transform.localRotation = rot;
        units.Add(unit.GetComponent<Unit>());
        return unit;
    }
}
