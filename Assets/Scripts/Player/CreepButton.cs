﻿using UnityEngine;
using System.Collections;
using System;

public class CreepButton : UnitButton
{

    public Creep creep;

    private enum State { IDLE, BUILDING, READY, DISABLED };
    private State state;
    private float buildProgress;

    private Player player;

    // Use this for initialization
    void Start()
    {
        player = gameObject.GetComponentInParent<Player>();
        state = State.IDLE;
    }

    // Update is called once per frame
    void Update()
    {
        CheckReqs();
        switch (state)
        {
            case State.DISABLED:
                gameObject.GetComponent<Renderer>().material.color = Color.gray;
                return;
            case State.IDLE:
                gameObject.GetComponent<Renderer>().material.color = Color.white;
                if (player.unitQueues[creep.type].Count > 0)
                {
                    if (player.unitQueues[creep.type].Peek() == creep) state = State.BUILDING;
                }
                return;
            case State.BUILDING:

                if (!player.unitQueues[creep.type].Contains(creep))
                {
                    state = State.IDLE;
                    return;
                }
               
                if (player.unitQueues[creep.type].Peek() == creep && player.buildingProgress >= 1)
                {
                    GetComponent<Renderer>().material.color = Color.green;
                    state = State.READY;
                    return;
                }
                if(player.unitQueues[creep.type].Peek() == creep)
                {
                    GetComponent<Renderer>().material.color = new Color(1 - player.unitProgresses[creep.type], player.unitProgresses[creep.type], 0);
                }
                return;
            case State.READY:
                    state = State.IDLE;
                return;
        }
    }

    private void CheckReqs()
    {
        if(player.buildings.Count<=0)
        {
            state = State.DISABLED;
            return;
        }
        foreach (Building building in player.buildings)
        {
            bool canBuild = false;
            foreach (Building req in creep.requisites)
            {
                if (req.buildName == building.buildName)
                {
                    canBuild = true;
                }
            }
            if (!canBuild)
            {
                state = State.DISABLED;
                return;
            }
        }
        if (state == State.DISABLED) state = State.IDLE;
        return;
    }

    public override void Press(Controller pressed)
    {
        if (state != State.DISABLED)
        {
            if (player.Build(creep))
            {
            }
        }
    }
    
}
