﻿using UnityEngine;
using Valve.VR;
using System.Collections;

public class Cabin : MonoBehaviour
{

    public GameObject cabinPrefab;
    public float height;
    private float width;
    private float depth;
    private GameObject cabin;

    // Use this for initialization
    void Start()
    {
        bool success = Valve.VR.OpenVR.Chaperone.GetPlayAreaSize(ref width, ref depth);
        cabin = Instantiate(cabinPrefab);
        cabin.transform.parent = gameObject.transform;
        cabin.transform.localPosition = new Vector3(0f, 0.05f, 0f);
        if (success) cabin.transform.localScale = new Vector3(width, height, depth);
        else cabin.transform.localScale = new Vector3(1.5f, 2.5f, 2f);

        GetComponent<Tracker>().unit.player = GetComponent<Player>();

        GameObject firstMap = (GameObject)Instantiate(GameController.instance.minimapPrefab, transform);
        firstMap.transform.localPosition = new Vector3(0f, 1.1f, -(width / 2) + .6f);
        firstMap.transform.localScale = new Vector3(0.07f, 0.07f, 0.07f);
        firstMap.transform.localRotation = Quaternion.FromToRotation(Vector3.forward, new Vector3(0,-1,0.7f));
        firstMap.GetComponent<MinimapProjector>().size = 10;
        int i = 0;
        foreach (GameObject buttonPrefab in GameController.instance.buttonPrefabs)
        {
            GameObject button = (GameObject)Instantiate(buttonPrefab, transform);
            button.transform.localPosition = new Vector3(i*0.5f, 0.5f, 0.5f);
            button.transform.localScale = Vector3.one * 0.4f;
            button.transform.localRotation = Quaternion.identity;
            i++;
        }

    }

    // Update is called once per frame
    void Update()
    {

    }
}
