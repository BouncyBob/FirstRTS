﻿using UnityEngine;
using System.Collections;
using System;

public class Controller : MonoBehaviour
{

    public GameObject selectBall;

    private Ray ray;
    private RaycastHit hit;
    private Player player;
    private int controllerIndex;
    private SteamVR_Controller.Device device;

    //select vars
    private GameObject selectArea;
    private Vector3 selectOrigin;
    private Vector3 selectPoint;

    //general map control
    private Minimap prevMap;
    private Minimap currentMap;

    //scrolling vars
    private Vector3 oldPoint;
    private Vector3 newPoint;

    private GameObject cursor;

    private GameObject cursorPrefab;

    //fung shuei stuff
    private GameObject holding;
    private Transform oldPos;
    private Transform curPos;
    private Quaternion oldRot;
    private Quaternion newRot;

    public enum State { DEFAULT, SELECTING, BUILDING, MAPMOVING, FUNGSHUEI, HOLDINGFURNITURE };
    public State state;


    // Use this for initialization
    void Start()
    {
        cursorPrefab = GameController.instance.cursorPrefab;
        player = gameObject.GetComponentInParent<Player>();
        controllerIndex = (int)gameObject.GetComponent<SteamVR_TrackedObject>().index;
        cursor = Instantiate(cursorPrefab);
        state = State.DEFAULT;
    }

    // Update is called once per frame
    void Update()
    {
        device = SteamVR_Controller.Input(controllerIndex);
        currentMap = null;
        if (prevMap == null) prevMap = currentMap;
        ray = new Ray(transform.position, transform.forward);
        if (Physics.Raycast(ray, out hit))
        {
            if (!hit.transform.GetComponentInParent<Minimap>())
            {
                Debug.Log(hit.transform.name);
                cursor.transform.position = Vector3.one * 999999;
                return;
            }
            currentMap = hit.transform.GetComponentInParent<Minimap>();
            cursor.transform.position = hit.point;
            switch (state)
            {
                case State.DEFAULT:
                    cursor.GetComponent<Renderer>().material.color = Color.white;
                    if (device.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
                    {
                        StartSelect(hit.point);
                        state = State.SELECTING;
                    }
                    else if (device.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
                    {
                        switch (hit.transform.name)
                        {
                            case ("MapCollider"):
                            case ("UnitCollider"):
                                player.Move(currentMap.transformPoint(hit.point));
                                break;
                            case ("ButtonCollider"):
                                hit.transform.GetComponentInParent<UnitButton>().Press(this);
                                break;
                            default:
                                break;
                        }
                    }
                    else if (device.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad))
                    {
                        oldPoint = currentMap.transform.InverseTransformPoint(hit.point);
                        state = State.MAPMOVING;
                    }
                    else if (device.GetPressDown(SteamVR_Controller.ButtonMask.ApplicationMenu))
                    {
                        state = State.FUNGSHUEI;
                    }
                    break;

                case (State.SELECTING):
                    cursor.GetComponent<Renderer>().material.color = Color.yellow;
                    if (currentMap == null || currentMap.id != prevMap.id)
                    {
                        StopSelect();
                        return;
                    }
                    if (device.GetPressUp(SteamVR_Controller.ButtonMask.Grip) || !device.GetPress(SteamVR_Controller.ButtonMask.Grip))
                    {
                        StopSelect();
                        return;
                    }
                    if (device.GetPressDown(SteamVR_Controller.ButtonMask.ApplicationMenu))
                    {
                        StopSelect();
                        state = State.FUNGSHUEI;
                        break;
                    }
                    KeepSelecting(hit.point);
                    break;

                case (State.BUILDING):
                    if (device.GetPress(SteamVR_Controller.ButtonMask.Grip))
                    {
                        state = State.DEFAULT;
                        return;
                    }
                    if (hit.transform.name == "MapCollider")
                    {
                        cursor.GetComponent<Renderer>().material.color = Color.cyan;
                        if (Unit.canSpawn(player.constructing.gameObject,
                                currentMap.transformPoint(hit.point)))
                        {
                            cursor.GetComponent<Renderer>().material.color = Color.green;
                            if (device.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
                            {
                                player.CreateBuilding(currentMap.transformPoint(hit.point));
                                state = State.DEFAULT;
                            }
                        }
                    }
                    if (device.GetPressDown(SteamVR_Controller.ButtonMask.ApplicationMenu))
                    {
                        state = State.FUNGSHUEI;
                    }
                    break;

                case (State.MAPMOVING):
                    foreach (Tracker tracker in currentMap.trackers)
                    {
                        tracker.transform.GetComponentInChildren<Collider>().enabled = false;
                    }
                    if (currentMap == null || currentMap.id != prevMap.id || currentMap == GameController.instance.bigMinimap.GetComponent<Minimap>() ||
                        device.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad) || !device.GetPress(SteamVR_Controller.ButtonMask.Touchpad))
                    {
                        foreach (Tracker tracker in currentMap.trackers)
                        {
                            tracker.transform.GetComponentInChildren<Collider>().enabled = true;
                        }
                        state = State.DEFAULT;
                        break;
                    }
                    if (device.GetPressDown(SteamVR_Controller.ButtonMask.ApplicationMenu))
                    {
                        state = State.FUNGSHUEI;
                        break;
                    }
                    newPoint = currentMap.transform.InverseTransformPoint(hit.point);
                    Vector3 distance = newPoint - oldPoint;
                    distance.y = 0;
                    currentMap.transform.localPosition += distance;
                    oldPoint = currentMap.transform.InverseTransformPoint(hit.point);
                    break;
                case (State.FUNGSHUEI):
                    cursor.GetComponent<Renderer>().material.color = Color.blue;

                    if (device.GetPress(SteamVR_Controller.ButtonMask.Trigger))
                    {
                        bool validHold = false;
                        if ((hit.transform.name == "MapCollider" || hit.transform.name == "UnitCollider") &&
                            hit.transform.GetComponentInParent<MinimapProjector>() != null)
                        {
                            validHold = true;
                            holding = hit.transform.GetComponentInParent<MinimapProjector>().gameObject;

                        }
                        else if (hit.transform.name == "ButtonCollider")
                        {
                            validHold = true;
                            holding = hit.transform.GetComponentInParent<UnitButton>().gameObject;
                        }
                        if (validHold)
                        {
                            holding.transform.parent = transform;
                            holding.transform.localPosition = Vector3.zero;
                            holding.transform.localRotation = Quaternion.identity;
                            state = State.HOLDINGFURNITURE;
                        }
                    }
                    if (device.GetPressDown(SteamVR_Controller.ButtonMask.ApplicationMenu))
                    {
                        state = State.DEFAULT;
                    }
                    break;

                default:
                    break;
            }
            prevMap = currentMap;
        }
        //holding furniture doesn't depend on pointing at something
        if (state == State.HOLDINGFURNITURE)
        {
            if (!device.GetPress(SteamVR_Controller.ButtonMask.Trigger))
            {
                holding.transform.parent = player.transform;
                state = State.FUNGSHUEI;
            }
            if (device.GetPressDown(SteamVR_Controller.ButtonMask.ApplicationMenu))
            {
                holding.transform.parent = player.transform;
                state = State.DEFAULT;
            }

        }
    }

    private void StartSelect(Vector3 point)
    {
        state = State.SELECTING;
        selectArea = (GameObject)Instantiate(selectBall, currentMap.transform);
        selectOrigin = currentMap.transform.InverseTransformPoint(point);

    }
    private void KeepSelecting(Vector3 point)
    {
        if (state != State.SELECTING) return;
        point = currentMap.transform.InverseTransformPoint(point);
        selectPoint = (point + selectOrigin) / 2;
        selectArea.transform.localPosition = selectPoint;
        float dist = Vector3.Distance(point, selectOrigin);
        selectArea.transform.localScale = Vector3.one * dist;
    }
    private void StopSelect()
    {
        if (state != State.SELECTING) return;
        ArrayList units = new ArrayList();
        float selectDist = prevMap.transform.TransformVector(selectArea.transform.localScale).x / 2;
        Collider[] selectedUnits = Physics.OverlapSphere(prevMap.transform.TransformPoint(selectPoint), selectDist);
        foreach (Collider collide in selectedUnits)
        {
            if (collide.name == "UnitCollider" && collide.GetComponentInParent<Minimap>().id == prevMap.id)
            {
                Tracker track = collide.GetComponentInParent<Tracker>();
                Debug.Log("Selected");
                units.Add(track.unit);
            }
        }
        if (units.Count > 0)
        {
            player.Select(units);
        }
        else
        {
            Debug.Log("Didn''t select");
        }
        GameObject.Destroy(selectArea);
        state = State.DEFAULT;
    }


}
