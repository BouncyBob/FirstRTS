﻿using UnityEngine;
using System.Collections;

public class BuildingButton : UnitButton
{

    public Building building;

    private enum State { IDLE, BUILDING, READY, DISABLED };
    private State state;
    private float buildProgress;

    private Player player;

    // Use this for initialization
    void Start()
    {
        player = gameObject.GetComponentInParent<Player>();
        state = State.IDLE;
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case State.IDLE:
                if (player.isConstructing)
                {
                    if (player.constructing.buildName == building.buildName) state = State.BUILDING;
                    else state = State.DISABLED;
                    return;
                }
                gameObject.GetComponent<Renderer>().material.color = Color.white;
                return;
            case State.BUILDING:
                if (!player.isConstructing)
                {
                    state = State.IDLE;
                    return;
                }
                if (player.constructing.buildName != building.buildName)
                {
                    state = State.DISABLED;
                    return;
                }
                if(player.buildingProgress>=1)
                {
                    GetComponent<Renderer>().material.color = Color.green;
                    state = State.READY;
                    return;
                }
                GetComponent<Renderer>().material.color = new Color(1 - player.buildingProgress, player.buildingProgress, 0);
                return;
            case State.READY:
                if (!player.isConstructing)
                {
                    state = State.IDLE;
                }
                return;
        }
    }
    public override void Press(Controller pressed)
    {
        if(state==State.IDLE)
        {
            if (player.Build(building))
            {
                state = State.BUILDING;
            }
        }
        else if(state==State.READY)
        {
            pressed.state = Controller.State.BUILDING;
        }
    }
    public virtual void onReady()
    {
        
    }
}
