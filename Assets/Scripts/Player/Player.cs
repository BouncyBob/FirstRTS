﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour
{

    public ArrayList selected
    {
        get
        {
            if (pselected == null) return new ArrayList();
            return pselected;
        }
        set
        {
            if (pselected != null)
            {
                foreach (Unit unit in selected)
                {
                    unit.selected = false;
                }
            }
            foreach (Unit unit in value)
            {
                unit.selected = true;
            }
            pselected = value;
        }
    }

    private ArrayList pselected;

    public Color color;

    public int money;

    public Building constructing;
    public float buildingProgress;
    public bool isConstructing;

    public Dictionary<Creep.Type, Queue<Creep>> unitQueues;
    public Dictionary<Creep.Type, Building> unitSpawns;
    public Dictionary<Creep.Type, float> unitProgresses;

    public List<Building> buildings;
    public List<Creep> creeps;

    void Start()
    {
        unitQueues = new Dictionary<Creep.Type, Queue<Creep>>();
        unitSpawns = new Dictionary<Creep.Type, Building>();
        unitProgresses = new Dictionary<Creep.Type, float>();
        foreach (Creep.Type type in Creep.types)
        {
            unitQueues.Add(type, new Queue<Creep>());
            unitSpawns.Add(type, null);
            unitProgresses.Add(type, 0);
        }
        isConstructing = false;
        money = 3000;
    }

    // Update is called once per frame
    void Update()
    {
        if (constructing != null && buildingProgress < 1)
        {
            buildingProgress += Time.deltaTime / constructing.buildTime;
        }
        foreach (Creep.Type type in Creep.types)
        {
            if (unitQueues[type].Count != 0)
            {
                unitProgresses[type] += Time.deltaTime / unitQueues[type].Peek().buildTime;
                if (unitProgresses[type] >= 1)
                {
                    CreateCreep(type);
                }
            }
        }
    }

    public void Select(Unit unit)
    {
        ArrayList units = new ArrayList();
        units.Add(unit);
        selected = units;
    }
    public void Select(ArrayList units)
    {
        selected = units;
    }

    public void Move(Vector3 pos)
    {

        foreach (Unit unit in selected)
        {
            unit.Move(pos);
        }
    }

    /* public void Create(GameObject unit,Vector3 pos)
     {
         Instantiate(unit, pos, Quaternion.identity,GameController.instance.map.transform.FindChild("Units"));
     }
     */

    public bool Build(Building building)
    {
        foreach (Building requisite in building.requisites)
        {
            bool reqFulfilled = false;
            foreach (Building build in buildings)
            {
                if (build.buildName == requisite.buildName)
                {
                    reqFulfilled = true;
                }
            }
            if (!reqFulfilled) return false;
        }
        if (money >= building.cost)
        {
            money -= building.cost;
            isConstructing = true;
            constructing = building;
            return true;
        }
        return false;
    }

    public bool Build(Creep creep)
    {
        foreach (Building requisite in creep.requisites)
        {
            bool reqFulfilled = false;
            foreach (Building build in buildings)
            {
                if (build.buildName == requisite.buildName)
                {
                    reqFulfilled = true;
                }
            }
            if (!reqFulfilled) return false;
        }
        if (money < creep.cost) return false;

        money -= creep.cost;
        unitQueues[creep.type].Enqueue(creep);
        return true;
    }

    //called when controller builds somewhere
    public void CreateBuilding(Vector3 pos)
    {
        if (constructing != null && buildingProgress >= 1 && Building.canSpawn(constructing.gameObject, pos))
        {
            GameObject newBuild = (GameObject)Instantiate(constructing.gameObject, pos, Quaternion.identity, GameController.instance.map.transform.FindChild("Units"));
            Building newBuilding = newBuild.GetComponent<Building>();
            newBuilding.player = this;
            buildings.Add(newBuilding);
            if (newBuilding.spawnFor != Creep.Type.NOTHING && unitSpawns[newBuilding.spawnFor]==null)
            {
                unitSpawns[newBuilding.spawnFor] = newBuilding;
            }
            constructing = null;
            buildingProgress = 0;
            isConstructing = false;
        }
    }

    //called when creep is done
    public void CreateCreep(Creep.Type type)
    {
        Creep creep = (Creep)unitQueues[type].Dequeue();


        Debug.Log(unitSpawns.Count);
        Debug.Log(unitSpawns[creep.type]);
        Debug.Log(unitSpawns[creep.type].transform.position);
        GameObject newCreep = (GameObject)Instantiate(creep.gameObject, unitSpawns[creep.type].transform.position,
            Quaternion.identity, GameController.instance.map.transform.FindChild("Units"));
        creeps.Add(newCreep.GetComponent<Creep>());
        unitProgresses[type] = 0;
        newCreep.GetComponent<Creep>().player = this;

    }
}
