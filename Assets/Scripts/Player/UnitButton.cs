﻿using UnityEngine;
using System.Collections;

public abstract class UnitButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public abstract void Press(Controller pressed);
}
