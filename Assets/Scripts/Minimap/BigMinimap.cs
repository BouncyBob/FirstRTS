﻿using UnityEngine;
using System.Collections;

public class BigMinimap : Minimap {


    public override void SpawnUnit(Unit mapUnit)
    {
        Tracker newTracker = Instantiate(mapUnit.worldPrefab).GetComponent<Tracker>();
        newTracker.gameObject.transform.parent = gameObject.transform.FindChild("Units");
        newTracker.minimap = this;
        newTracker.unit = mapUnit;
        newTracker.transform.localScale = Vector3.one;
    }
}
