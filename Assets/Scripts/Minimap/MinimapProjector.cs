﻿using UnityEngine;
using System.Collections;

public class MinimapProjector : MonoBehaviour {

    public Material minimapMaterial;
    public float size;
    public Color colour;

    private Transform mapCollider;

	// Use this for initialization
	void Start () {
        mapCollider =transform.FindChild("Minimap").FindChild("MapCollider");
     }

    // Update is called once per frame
    void Update () {
        //make sure MapCollider is in the right place, has to be a parent of minimap for scripting
        mapCollider.SetParent(transform);
        mapCollider.localScale = new Vector3(size * 2, 0.05f, size * 2);
        mapCollider.localPosition = Vector3.zero;
        mapCollider.parent = transform.Find("Minimap");
        
        foreach (Transform child in transform.FindChild("Minimap").FindChild("Geometry"))
        {
            //start is called too early to set material there
            child.GetComponent<Renderer>().material = minimapMaterial;
            child.GetComponent<Collider>().enabled = false;
            child.GetComponent<Renderer>().material.SetFloat("_Radius", (size * transform.localScale.x));
            child.GetComponent<Renderer>().material.SetColor("_Color", colour);
            child.GetComponent<Renderer>().material.SetVector("_Point", transform.position);
        }
        foreach (Transform child in transform.FindChild("Minimap").FindChild("Units"))
        {
            //TODO: color referenced by player, enable/disable colliders depending on distance from centre
            //start is called too early to set material there
            child.FindChild("Render").GetComponent<Renderer>().material = minimapMaterial;
            child.FindChild("Render").GetComponent<Renderer>().material.SetFloat("_Radius", (size * transform.localScale.x));
            child.FindChild("Render").GetComponent<Renderer>().material.SetColor("_Color", child.GetComponent<Tracker>().unit.player.color);
            child.FindChild("Render").GetComponent<Renderer>().material.SetVector("_Point", transform.position);
        }

    }
}
