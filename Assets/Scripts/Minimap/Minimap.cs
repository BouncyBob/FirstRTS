﻿using System;
using System.Collections;
using UnityEngine;

public class Minimap : MonoBehaviour {

    private GameController gameController;
    private Map map;
    public ArrayList trackers;
    private static int ids;
    public int id;

    public virtual void SpawnUnit(Unit mapUnit)
    {
        Tracker newTracker = Instantiate(mapUnit.trackerPrefab).GetComponent<Tracker>();
        newTracker.transform.parent = gameObject.transform.FindChild("Units");
        newTracker.minimap = this;
        newTracker.unit = mapUnit;
        newTracker.transform.localScale = Vector3.one;
        trackers.Add(newTracker);
    }


    // Use this for initialization
    void Start () {
        id = (ids++);
        gameController = GameController.instance;
        map = gameController.map;
        map.minimaps.Add(this);
        trackers = new ArrayList();
        //get map geometry from main map  
        for(int i=0; i< map.transform.FindChild("Geometry").childCount;i++)
        {
            GameObject mapGeoComponent = map.transform.FindChild("Geometry").GetChild(i).gameObject;
            GameObject geoComponent = (GameObject)Instantiate(mapGeoComponent, Vector3.zero,mapGeoComponent.transform.rotation*transform.rotation);
            geoComponent.transform.SetParent(transform.FindChild("Geometry"));
            geoComponent.transform.localPosition = mapGeoComponent.transform.localPosition;
            geoComponent.transform.localScale = mapGeoComponent.transform.localScale;
            geoComponent.transform.localRotation = mapGeoComponent.transform.localRotation;
        } 
        //get all units currently in play ; exception for player's main unit on main minimap
        foreach(Unit unit in map.units)
        {
            SpawnUnit(unit);
        }
        /*
        for(int i=0;i< map.transform.FindChild("Units").childCount; i++)
        {
            if (!(gameObject == GameController.instance.bigMinimap && map.transform.FindChild("Units").GetChild(i).name == "PlayerCabinUnit"))
            SpawnUnit(map.units);
        }
        */
	}

    

    // Update is called once per frame
    void Update () {
	    
	}

    internal Vector3 transformPoint(Vector3 point)
    {
        return transform.InverseTransformPoint(point);
    }
}
