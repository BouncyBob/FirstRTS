﻿using UnityEngine;
using System.Collections;

public class Tracker : MonoBehaviour {

    public Minimap minimap;
    public Unit unit;
    private static int ids;
    public int id;

	// Use this for initialization
	void Start () {
        id = (ids++);
	}
	
	// Update is called once per frame
	void Update () {
        if (unit == null) return;
        transform.localPosition = unit.transform.localPosition;
        transform.localRotation = unit.transform.localRotation;
        transform.localScale = Vector3.one;
	}

/*    void Move(Vector3 minimapPos)
    {
        
        Vector3 mapPos = minimap.transform.InverseTransformPoint(minimapPos);
        unit.GetComponent<Unit>().Move(minimapPos);
    }*/
}
