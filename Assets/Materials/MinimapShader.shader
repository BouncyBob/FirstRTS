﻿Shader "Custom/MinimapShader" {
	Properties {
		_Color ("Colour", Color) = (1,1,1,1)
		_Point ("Centre", Vector) = (0,0,0,0)
		_Radius ("Radius",Float) = 1.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		Cull Off
		CGPROGRAM
		#pragma surface surf Lambert
		struct Input {
			float3 worldPos;
		};
		float4 _Color;
		float3 _Point;
		float _Radius;
		void surf (Input IN, inout SurfaceOutput o) {
			clip(_Radius>= 
				sqrt(
					(sqrt( (IN.worldPos.x-_Point.x)*(IN.worldPos.x - _Point.x)
						+ (IN.worldPos.y - _Point.y)*(IN.worldPos.y - _Point.y)
					) 
					*
					sqrt((IN.worldPos.x - _Point.x)*(IN.worldPos.x - _Point.x)
						+ (IN.worldPos.y - _Point.y)*(IN.worldPos.y - _Point.y)
					))
					+ (IN.worldPos.z - _Point.z)*(IN.worldPos.z - _Point.z)
				)
				? 1:-1
			);//big-ass 3d trigonometric line, RIP my card
			o.Albedo = _Color.rgb;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
