Very basic RTS project to get myself going with VR in general
-box should conform to VR chaperone area
-create, select and move units around

controls:

-grip button selects, trigger moves,  touchpad moves the minimap

-menu button changes to room arrangement mode, trigger to move items around

 -menu button again to change back

the cubes are building buttons, one builds a barracks to place and the other
builds units from the barracks.

you have 30 credits; a barracks costs 10 and a unit costs 1. 
No way of getting more.